FROM alpine

LABEL maintainer="sandroandrade@ifba.edu.br"

RUN apk update && \
    apk upgrade && \
    apk add build-base git cmake && \
    rm -rf /var/cache/apk/*

RUN git clone https://github.com/zeromq/libzmq.git && \
    cd libzmq && git checkout v4.3.2 && mkdir build && cd build && \
    cmake ../ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/ -DBUILD_TESTS=OFF && \
    make -j 5 && make install && cd ../../ && rm -rf libzmq

RUN git clone https://github.com/zeromq/cppzmq.git && \
    cd cppzmq && git checkout v4.6.0 && mkdir build && cd build && \
    cmake ../ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/ -DCPPZMQ_BUILD_TESTS=OFF && \
    make install && cd ../../ && rm -rf cppzmq

# Set environment variables.
ENV HOME /var/server

# Define working directory.
COPY ./* /var/server/

WORKDIR /var/server

RUN g++ -o rrbroker rrbroker.cpp -lzmq && rm -rf rrbroker.cpp

EXPOSE 5559
EXPOSE 5560

ENV LD_LIBRARY_PATH=/usr/lib64/

CMD ["/var/server/rrbroker"]
